#define SKYMOTE_STRING		 "mspmote.SkyMoteType"

#define SENSOR_SOURCE_PATH	 "tools/stm32w/udp-ipv6-sleep/udp_sensor.c"
#define SENSOR_COMMANDS		 "make udp_sensor.sky TARGET=sky"
#define SENSOR_FIRMWARE_PATH	 "tools/stm32w/udp-ipv6-sleep/udp_sensor.sky"

#define ACTUATOR_SOURCE_PATH	 "tools/stm32w/udp-ipv6-sleep/udp_actuator.c"
#define ACTUATOR_COMMANDS	 "make udp_actuator.sky TARGET=sky"
#define ACTUATOR_FIRMWARE_PATH	 "tools/stm32w/udp-ipv6-sleep/udp_actuator.sky"

#define CONTROLER_SOURCE_PATH	 "tools/stm32w/udp-ipv6-sleep/udp_controler.c"
#define CONTROLER_COMMANDS	 "make udp_controler.sky TARGET=sky"
#define CONTROLER_FIRMWARE_PATH	 "tools/stm32w/udp-ipv6-sleep/udp_controler.sky"

#define WALLSWITCH_SOURCE_PATH	 "tools/stm32w/udp-ipv6-sleep/udp_wallswitch.c"
//#define WALLSWITCH_COMMANDS	 "make udp_wallswitch.sky TARGET=sky"
#define WALLSWITCH_FIRMWARE_PATH "tools/stm32w/udp-ipv6-sleep/udp_wallswitch.sky"

#define SIMULATION_TIME		1200000

#define LOGOUTPUT		40000
#define SUCCESS_RATIO_TX	1.0
#define SUCCESS_RATIO_RX	1.0
#define TRANSMISSION_RANGE	5.0
#define	INTERFERENCE_RANGE	8.0
#define RADIOMEDIUM		"UDGM"
#define MOTE_DELAY		0.0
#define DELAY_TIME		0.0
#define TITLE			"Automatically generated simulation by coojaTopGen"
#define VERSION			"1.0"
#define ENCODING		"UTF-8"




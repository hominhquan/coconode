#include "topologie_ctrl.h"


struct node *node;
struct point *t;
struct data_thread *dt;


void generate_topologie (int number_of_sensors, int percentage_sensors, 
		int percentage_actuators)
{
	double x_max = DBL_MIN;
	double x_min = DBL_MAX;
	double y_min = DBL_MAX;
	double y_max = DBL_MIN;
	int i = 0;
	void* list;
	printf("%d %d %d \n",number_of_sensors, percentage_sensors, 
		      percentage_actuators);
	
	list = generate_nodes (number_of_sensors, percentage_sensors, 
		      percentage_actuators);
	t = malloc(number_of_sensors * sizeof(struct point));
	
	while ((node = (struct node *) das_traverse(list)) != NULL) {
	  if(i < number_of_sensors){
	    
	    if(x_max < node->data.x) {
	      x_max = node->data.x;
	    }
	    else if(x_min > node->data.x){
	      x_min = node->data.x;
	    }
	    if(y_max < node->data.y) {
	      y_max = node->data.y;
	    }
	    else if(y_min > node->data.y){
	      y_min = node->data.y;
	    }
	    t[i].id = node->data.id;
	    t[i].x = node->data.x;
	    t[i].y = node->data.y;
	    
	  }
	  else{
	    // ERREUR
	  }
	  i++;
	}
	
	x_max = x_max - x_min;
	y_max = y_max - y_min;
	for(i=0;i<number_of_sensors;i++){
	  t[i].x = (t[i].x - x_min) / x_max;
	  t[i].y = (t[i].y - y_min) / y_max;
	}
	dt = malloc(sizeof(struct data_thread));
	dt->data = malloc(number_of_sensors * sizeof(struct point));
	memcpy (dt->data,t,number_of_sensors* sizeof(struct point));
	struct point *tmp = (struct point *)dt->data;
	printf("%lu %lu \n",tmp[0].id, tmp[1].id);
	dt->length = number_of_sensors;
 	g_idle_add((GSourceFunc) callback_generate,dt);
}
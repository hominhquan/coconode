#ifndef __JSONNER_H__
#define __JSONNER_H__

GSList* get_generator_files();

GtkListStore* get_generator_files_list();

void update_generator_files_list(GtkTextView *textview, GSList* newlist);

void cb_menu_new_simulation (GtkWidget* widget, gpointer* data);
void cb_menu_export_config (GtkWidget* widget, gpointer* data);
void cb_menu_import_config (GtkWidget* widget, gpointer* data);

// void export_param(char* param_filename, char* path);
// void import_param(char* param_filename, char* path);

#endif


#ifndef H_CERCLE
#define H_CERCLE

#include <stdio.h>
#include <gtk/gtk.h>
#include <pthread.h>
#include "topologie_ctrl.h"

struct point {
    signed long int id;
    double x;
    double y;
};

struct data_thread {
    void *data;
    int length;
};

void quit ();
void generate();
gboolean eventDraw(GtkWidget *widget,
        GdkEvent *event,gpointer data);
static void drawArcs(GtkWidget *widget,gint y,gint fill);
void drawCircle(gint x, gint y,gint fill);
gint eventDelete(GtkWidget *widget,
        GdkEvent *event,gpointer data);
gboolean callback_generate (void *data);
gint eventDestroy(GtkWidget *widget,
        GdkEvent *event,gpointer data);

#endif
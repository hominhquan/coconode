#ifndef __MAIN_WINDOW_H__
#define __MAIN_WINDOW_H__

#include <stdio.h>
#include <glib.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <pthread.h>
#include <cairo.h>
#include <math.h>
#include <time.h>
#include "../topo_generator/topologie_ctrl.h"
#include "../controller/simulation_control.h"
#include "../jsonner/jsonner.h"

#define TO_CHILD dad2child[1]
#define FROM_DAD dad2child[0]

#define TO_DAD child2dad[1]
#define FROM_CHILD child2dad[0]

#define START 1
#define PAUSE 2
#define STOP 3
#define RESTART 4  

// if defined, scheduled simulation will start in 1 second 
// instead of 1 minutes : 
#define BEGIN_BURST 1 

struct point {
    int id;
    double x;
    double y;
    char *type;
};

struct data_thread {
    void *data;
    int length;
};

typedef struct {
  char *type;
  gchar * color;
} type_to_color;

typedef struct {
  gint nb_nodes;
  gdouble percentage_sensors;
  gdouble percentage_actuators;  
  GSList* files;
  gboolean new_files;
  gchar* repository;
} Generation_infos;

typedef struct {
  int minutes;
  int repeat;
  int rang;
  gchar *protocol;
  gchar *repository;
  int nodes;
  time_t scheduled_date; // in seconds
} Start_infos;

typedef struct {
  GIOChannel *toDad;
  GIOChannel *fromDad;
} talk2Dad;

typedef struct {
  GIOChannel *toChild;
  GIOChannel *fromChild;
} talk2Child;

typedef struct{
  char* contiki_file;
  char* protocol; 
} Cooja_infos;

void quit ();
void cb_clicked();
void generate();
gboolean eventDraw(GtkWidget *widget,
        GdkEvent *event,gpointer data);
void drawCircle(gint x, gint y,gint fill);
gboolean maj_loadBar (void *t);
gboolean maj_eclapsed_time (void *t);
gboolean callback_generate (void *data);
void start ();
void pause_simul();
void restart_simul();
void stop_simul();
int comparator_simul_data (Start_infos* sorted_data, Start_infos* new_data, gpointer user_data);

#endif


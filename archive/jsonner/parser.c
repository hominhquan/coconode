#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <glib-object.h>
#include <json-glib/json-glib.h>

int main (int argc, char *argv[])
{
    JsonParser *parser;
    // JsonNode *root;
    GError *error = NULL;
    GSList* generator_files = NULL;

    // check for argument:
    if (argc < 2)
      {
        g_print ("Usage: ./parser <filename.json>\n");
        return EXIT_FAILURE;
      }
    // init:
    g_type_init ();

    // create a parser:
    parser = json_parser_new ();

    // load from JSON file and check for error
    json_parser_load_from_file (parser, argv[1], &error);
    if (error)
      {
        g_print ("Unable to parse `%s': %s\n", argv[1], error->message);
        g_error_free (error);
        g_object_unref (parser);
        return EXIT_FAILURE;
      }

    // create a reader from parser:
    JsonReader *reader = json_reader_new (json_parser_get_root (parser));

    // begin read simulation:
    json_reader_read_member (reader, "simulation");
      // ----duration:
      json_reader_read_member (reader, "duration");
        // ------hours:
        json_reader_read_member (reader, "hours");
          int hours = json_reader_get_int_value (reader);        
        json_reader_end_member (reader);
        // ------minutes:
        json_reader_read_member (reader, "minutes");
          int minutes = json_reader_get_int_value (reader);
        json_reader_end_member (reader);
      json_reader_end_member (reader);

      // ----repeat:
      json_reader_read_member (reader, "repeat");
        int repeat = json_reader_get_int_value (reader);
      json_reader_end_member (reader);

      // ----protocol:
      json_reader_read_member (reader, "protocol");
        const char* protocol = json_reader_get_string_value (reader);
      json_reader_end_member (reader);

      // ----beginin:
      json_reader_read_member (reader, "beginin");
        // ------schedule_hours:
        json_reader_read_member (reader, "schedule_hours");
          int schedule_hours = json_reader_get_int_value (reader);
        json_reader_end_member (reader);
        // ------schedule_minutes:
        json_reader_read_member (reader, "schedule_minutes");
          int schedule_minutes = json_reader_get_int_value (reader);
        json_reader_end_member (reader);
      json_reader_end_member (reader);
    json_reader_end_member (reader);
    // end read simulation

    // begin read topology:
    json_reader_read_member (reader, "topology");
      // ----nb_nodes:
      json_reader_read_member (reader, "nb_nodes");
        int nb_nodes = json_reader_get_int_value (reader);
      json_reader_end_member (reader);

      // ----sensor_percent:
      json_reader_read_member (reader, "sensor_percent");
        int sensor_percent = json_reader_get_int_value (reader);
      json_reader_end_member (reader);

      // ----actuator_percent:
      json_reader_read_member (reader, "actuator_percent");
        int actuator_percent = json_reader_get_int_value (reader);
      json_reader_end_member (reader);

      // ----generator_files:
      json_reader_read_member (reader, "generator_files");
        int i;
        int array_size = json_reader_count_elements(reader);
        const char* tmp;
        for (i = 0; i < array_size; i++){
          json_reader_read_element (reader, i);
          tmp = json_reader_get_string_value (reader);
          json_reader_end_element (reader);
          generator_files = g_slist_append(generator_files, (gpointer) tmp);
        }
      json_reader_end_member (reader);

    json_reader_end_member (reader);
    // end read topology

    //TEST
    printf("Hours : %d\nMinutes : %d\nRepeat : %d\nProtocol : %s\nSchedule_hours : %d\n", hours, minutes, repeat, protocol, schedule_hours);
    printf("Schedule_minutes : %d\nNb_nodes : %d\nSensor_pctg : %d\nActuator_pctg : %d\n", schedule_minutes, nb_nodes, sensor_percent, actuator_percent);
    printf("Generator files : \n");
    for(i = 0; i < g_slist_length(generator_files); i++){
      printf("\t%s \n", (char*)g_slist_nth_data(generator_files, i));
    }


    // root = json_parser_get_root (parser);

    /* manipulate the object tree and then exit */
    g_object_unref (reader);
    g_object_unref (parser);

    return EXIT_SUCCESS;
}

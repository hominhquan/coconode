#include <gtk/gtk.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>


GtkWidget *label_time;
GError    *error = NULL;
static gpointer thread_time_func(gpointer data)
{
    time_t a=NULL;
    struct tm* time;
    int h,m,s;
    s = 0;
    while(1)
    {
	gchar *markup_time = g_markup_printf_escaped ("Time %02d",s);
	sleep(1);
	s++;
	gdk_threads_enter();
	gtk_label_set_text(GTK_LABEL(data), markup_time );
	g_free(markup_time);
	printf("Le thread est termine\n");
	gdk_threads_leave();
    }
}

void generate (){
   thread_time = g_thread_create( thread_time_func, (gpointer)label_time, FALSE, &error );
}
 
int main (int argc, char *argv[])
{
    GtkWidget *win, *frame, *button;
    GThread   *thread_time;
    
    if(!g_thread_supported())
      g_thread_init( NULL );
    gdk_threads_init();
    gdk_threads_enter();
    gtk_init (&argc, &argv);
 
    win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_container_set_border_width (GTK_CONTAINER (win), 8);
    gtk_window_set_title (GTK_WINDOW (win), "GTK  Multi-threading");
    gtk_window_set_position (GTK_WINDOW (win), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(win), 200, 100);
    g_signal_connect (win, "destroy", gtk_main_quit, NULL);
    
    frame = gtk_fixed_new();
    gtk_container_add(GTK_CONTAINER(win), frame);
    
    label_time = gtk_label_new("Loading ...");
    gtk_fixed_put(GTK_FIXED(frame), label_time, 20, 20);
    
    button = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
    gtk_widget_set_size_request(button, 95, 30);
    gtk_fixed_put(GTK_FIXED(frame), button, 100, 50);
    g_signal_connect (button, "clicked", generate, NULL);
    gtk_widget_show_all (win);
    gtk_main();
    gdk_threads_leave();
    return 0;
}